package org.hspconsortium.bilirubin.monitor.service;


import ca.uhn.fhir.rest.gclient.IRead;
import ca.uhn.fhir.rest.gclient.IReadExecutable;
import ca.uhn.fhir.rest.gclient.IReadTyped;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Type;
import org.hspconsortium.client.auth.credentials.Credentials;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NewbornBilirubinNotificationServiceTest {

    @Mock
    ClientCredentialsSessionFactory<? extends Credentials> mockEhrSessionFactory;

    @Mock
    Session mockSession;

    @Mock
    IRead mockIRead;

    @Mock
    IReadTyped mockIReadType;

    @Mock
    IReadExecutable mockIReadExecutable;

    @Mock
    Patient mockPatient;

    @Mock
    Observation mockObservation;

    @Mock
    Type mockDatatype;

    @InjectMocks
    NewbornBilirubinNotificationService newbornBilirubinNotificationService = new NewbornBilirubinNotificationService.Impl();

    @Test
    public void testHealth() {
        newbornBilirubinNotificationService.health();
    }

    @Test
    public void testDoAlertWhenNoBilirubinForNewbornInTimeFrame() throws Exception {
        IdType patientId = new IdType("patientId");
        Mockito.when(mockEhrSessionFactory.createSession()).thenReturn(mockSession);
        Mockito.when(mockSession.read()).thenReturn(mockIRead);
        Mockito.when(mockIRead.resource(Mockito.any(Class.class))).thenReturn(mockIReadType);
        Mockito.when(mockIReadType.withId(patientId)).thenReturn(mockIReadExecutable);
        Mockito.when(mockIReadExecutable.execute()).thenReturn(mockPatient);

        newbornBilirubinNotificationService.doAlertWhenNoBilirubinForNewbornInTimeFrame(patientId);
    }

    @Test
    public void testDoAlertWhenBilirubinValueOutsideThreshold() throws Exception {
        IdType observationId = new IdType("observationId");
        Mockito.when(mockEhrSessionFactory.createSession()).thenReturn(mockSession);
        Mockito.when(mockSession.read()).thenReturn(mockIRead);
        Mockito.when(mockIRead.resource(Mockito.any(Class.class))).thenReturn(mockIReadType);
        Mockito.when(mockIReadType.withId(observationId)).thenReturn(mockIReadExecutable);
        Mockito.when(mockIReadExecutable.execute()).thenReturn(mockObservation);
        Mockito.when(mockObservation.getValue()).thenReturn(mockDatatype);
        Mockito.when(mockDatatype.isEmpty()).thenReturn(true);

        newbornBilirubinNotificationService.doAlertWhenBilirubinValueOutsideThreshold(observationId);
    }
}