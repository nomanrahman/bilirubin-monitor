package org.hspconsortium.bilirubin.monitor;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IRestfulClientFactory;
import org.hspconsortium.bilirubin.monitor.service.NewbornBilirubinNotificationService;
import org.hspconsortium.bilirubin.monitor.service.PatientViewCdsHooksService;
import org.hspconsortium.client.auth.Scopes;
import org.hspconsortium.client.auth.SimpleScope;
import org.hspconsortium.client.auth.access.AccessTokenProvider;
import org.hspconsortium.client.auth.access.JsonAccessTokenProvider;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.auth.credentials.Credentials;
import org.hspconsortium.client.controller.FhirEndpointsProvider;
import org.hspconsortium.client.controller.FhirEndpointsProviderSTU3;
import org.hspconsortium.client.session.ApacheHttpClientFactory;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;
import org.hspconsortium.platform.messaging.converter.ResourceStringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;

import javax.inject.Inject;

@Configuration
@ImportResource("classpath*:/META-INF/spring/spring-integration-config.xml")
public class AppConfig {

    @Autowired
    private Environment env;

    @Bean
    public String fhirServicesUrl() {
        return env.getProperty("CONTEXT_FHIR_ENDPOINT");
    }

    @Bean
    public String clientId() {
        return env.getProperty("bilirubin-monitor.clientId");
    }

    @Bean
    public String scope() {
        return env.getProperty("bilirubin-monitor.scopes");
    }

    @Bean
    public String clientSecret() {
        return env.getProperty("bilirubin-monitor.clientSecret");
    }

    @Bean
    public Integer httpConnectionTimeOut() {
        return Integer.parseInt(env.getProperty("example.httpConnectionTimeoutMilliSeconds"
                , IRestfulClientFactory.DEFAULT_CONNECT_TIMEOUT + ""));
    }

    @Bean
    public Integer httpReadTimeOut() {
        return Integer.parseInt(env.getProperty("example.httpReadTimeoutMilliSeconds"
                , IRestfulClientFactory.DEFAULT_CONNECTION_REQUEST_TIMEOUT + ""));
    }

    @Bean
    public String proxyPassword() {
        return System.getProperty("http.proxyPassword", System.getProperty("https.proxyPassword"));
    }

    @Bean
    public String proxyUser() {
        return System.getProperty("http.proxyUser", System.getProperty("https.proxyUser"));
    }

    @Bean
    public Integer proxyPort() {
        return Integer.parseInt(System.getProperty("http.proxyPort", System.getProperty("https.proxyPort", "8080")));
    }

    @Bean
    public String proxyHost() {
        //-Dhttp.proxyHost=proxy.host.com -Dhttp.proxyPort=8080  -Dhttp.proxyUser=username -Dhttp.proxyPassword=password
        return System.getProperty("http.proxyHost", System.getProperty("https.proxyHost"));
    }

    @Bean
    public Long jsonTokenDuration() {
        return Long.parseLong(env.getProperty("example.tokenDuration", "900"));
    }

    @Bean
    @Inject
    public ClientSecretCredentials clientSecretCredentials(String clientSecret) {
        return new ClientSecretCredentials(clientSecret);
    }

    @Inject
    @Bean
    public ApacheHttpClientFactory apacheHttpClientFactory(String proxyHost, Integer proxyPort, String proxyUser,
                                                           String proxyPassword, Integer httpConnectionTimeOut, Integer httpReadTimeOut) {
        return new ApacheHttpClientFactory(proxyHost, proxyPort, proxyUser, proxyPassword, httpConnectionTimeOut, httpReadTimeOut);
    }

    @Inject
    @Bean
    public AccessTokenProvider tokenProvider(ApacheHttpClientFactory apacheHttpClientFactory) {
        return new JsonAccessTokenProvider(apacheHttpClientFactory);
    }

    @Bean
    public FhirEndpointsProvider fhirEndpointsProviderSTU3(FhirContext fhirContext) {
        return new FhirEndpointsProviderSTU3(fhirContext);
    }

    @Bean
    @Inject
    public Credentials credentials(String clientSecret) {
        return clientSecretCredentials(clientSecret);
    }

    @Bean
    public FhirContext fhirContext(Integer httpConnectionTimeOut, Integer httpReadTimeOut
            , String proxyHost, Integer proxyPort
            , String proxyUser, String proxyPassword) {
        FhirContext hapiFhirContext = FhirContext.forDstu3();
        // Set how long to try and establish the initial TCP connection (in ms)
        hapiFhirContext.getRestfulClientFactory().setConnectTimeout(httpConnectionTimeOut);

        // Set how long to block for individual read/write operations (in ms)
        hapiFhirContext.getRestfulClientFactory().setSocketTimeout(httpReadTimeOut);

        if (proxyHost != null) {
            hapiFhirContext.getRestfulClientFactory().setProxy(proxyHost, proxyPort);

            hapiFhirContext.getRestfulClientFactory().setProxyCredentials(proxyUser
                    , proxyPassword);
        }
        return hapiFhirContext;
    }

    @Bean
    @Inject
    public ClientCredentialsSessionFactory<? extends Credentials> ehrSessionFactory(
            FhirContext fhirContext, AccessTokenProvider tokenProvider, FhirEndpointsProvider fhirEndpointsProvider, String fhirServicesUrl,
            String clientId, Credentials credentials, String scope) {
        Scopes scopes = new Scopes();
        scopes.add(new SimpleScope(scope));
        return new ClientCredentialsSessionFactory<>(fhirContext, tokenProvider, fhirEndpointsProvider, fhirServicesUrl, clientId,
                credentials, scopes);
    }

    @Bean
    public String emailUsername() {
        return env.getProperty("email.username");
    }

    @Bean
    public String emailPassword() {
        return env.getProperty("email.password");
    }

    @Bean
    public String emailTo() {
        return env.getProperty("email.to");
    }

    @Bean
    public String emailFrom() {
        return env.getProperty("email.from");
    }

    @Bean
    public NewbornBilirubinNotificationService newbornBilirubinNotificationService() {
        return new NewbornBilirubinNotificationService.Impl();
    }

    @Bean
    public PatientViewCdsHooksService patientViewCdsHooksService() {
        return new PatientViewCdsHooksService.Impl();
    }

    @Bean
    public ResourceStringConverter resourceStringConverter() {
        return new ResourceStringConverter();
    }
}
