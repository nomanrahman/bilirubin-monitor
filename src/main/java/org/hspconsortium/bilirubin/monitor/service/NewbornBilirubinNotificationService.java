package org.hspconsortium.bilirubin.monitor.service;

import ca.uhn.fhir.model.api.IDatatype;
import org.apache.commons.lang.Validate;
import org.hl7.fhir.dstu3.model.*;
import org.hspconsortium.client.auth.credentials.Credentials;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Timer;

public interface NewbornBilirubinNotificationService {

    String health();

    String doAlertWhenNoBilirubinForNewbornInTimeFrame(IdType patientId);

    String doAlertWhenBilirubinValueOutsideThreshold(IdType bilirubinId);

    @Service
    class Impl implements NewbornBilirubinNotificationService {

        public static final String BILIRUBIN_CODE = "58941-6";

        public static final String SUCCESS = "SUCCESS";

        @Inject
        ClientCredentialsSessionFactory<? extends Credentials> ehrSessionFactory;

        @Inject
        String emailUsername;

        @Inject
        String emailPassword;

        @Inject
        String emailTo;

        @Inject
        String emailFrom;

        @Override
        public String health() {
            return ehrSessionFactory != null ? "OK" : "Not Initialized";
        }

        @Override
        public String doAlertWhenNoBilirubinForNewbornInTimeFrame(IdType patientId) {
            Validate.notNull(patientId);

            Session session = ehrSessionFactory.createSession();

            // find the patient record
            Patient patient = session.read().resource(Patient.class).withId(patientId).execute();

            Validate.notNull(patient);

            if (patient.getBirthDate() != null) {
                createTimerIfNewbornPatient(patient);
            }

            return SUCCESS;
        }

        private void createTimerIfNewbornPatient(Patient patient) {
            // if this patient is a newborn then schedule a timer task
            Date now = new Date();
            long patientAgeHours = (now.getTime() - patient.getBirthDate().getTime()) / (60 * 60 * 1000);

            // less than 5 days old?
            if (patientAgeHours <= (24 * 5)) {
                Timer timer = new Timer();
                // should create a timer task that goes off 12 hours after birth
                long twelveHoursAfterBirth = (patient.getBirthDate().getTime() + (12 * 60 * 60 * 1000));
                long delay = (twelveHoursAfterBirth - now.getTime());
                if (delay < 0) {
                    delay = 0;
                }
                timer.schedule(
                        new NewbornBilirubinTimerTask(ehrSessionFactory, patient, emailUsername, emailPassword,
                                emailTo, emailFrom),
                        delay);
            }
        }

        @Override
        public String doAlertWhenBilirubinValueOutsideThreshold(IdType bilirubinId) {
            Validate.notNull(bilirubinId);

            // try to just get the patient
            Session session = ehrSessionFactory.createSession();

            // this is just for testing the ehrSessionFactory, might not be real implementation
            Observation bilirubin = session.read().resource(Observation.class).withId(bilirubinId).execute();

            // if no bilirubin, then alert
            createAlertIfOutsideThreshold(bilirubin);

            return SUCCESS;
        }

        private void createAlertIfOutsideThreshold(Observation bilirubin) {
            if (bilirubin != null && !bilirubin.getValue().isEmpty()) {
                Type datatype = bilirubin.getValue();
                if (datatype instanceof Quantity) {
                    Quantity bilirubinQuantity = (Quantity) datatype;
                    // greater than 10.0
                    if (bilirubinQuantity.getValue().compareTo(BigDecimal.valueOf(12.0)) > 0) {
                        // matches observation id 16468
                        EmailService.sendEmail(
                                emailUsername,
                                emailPassword,
                                emailTo,
                                emailFrom,
                                "ALERT: Bilirubin outside threshold",
                                "<h1>Bilirubin observation value of: "
                                        + ((Quantity) bilirubin.getValue()).getValue()
                                        + " is outside threshold</h1>"
                        );
                    }
                }
            }
        }
    }
}
