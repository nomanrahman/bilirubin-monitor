package org.hspconsortium.bilirubin.monitor.service;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hspconsortium.client.auth.credentials.Credentials;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;

import java.util.TimerTask;

public class NewbornBilirubinTimerTask extends TimerTask {

    private ClientCredentialsSessionFactory<? extends Credentials> ehrSessionFactory;

    private Patient patient;

    private String emailUsername;

    private String emailPassword;

    private String emailTo;

    private String emailFrom;

    public NewbornBilirubinTimerTask(ClientCredentialsSessionFactory<? extends Credentials> ehrSessionFactory,
                                     Patient patient, String emailUsername, String emailPassword, String emailTo,
                                     String emailFrom) {
        this.ehrSessionFactory = ehrSessionFactory;
        this.patient = patient;
        this.emailUsername = emailUsername;
        this.emailPassword = emailPassword;
        this.emailTo = emailTo;
        this.emailFrom = emailFrom;
    }

    @Override
    public void run() {
        // try to just get the patient
        Session session = ehrSessionFactory.createSession();

        // this is just for testing the ehrSessionFactory, might not be real implementation
        Bundle bilirubinBundle =
                session
                        .search()
                        .forResource(Observation.class)
                        .where(Observation.PATIENT.hasId(patient.getId()))
                        .where(Observation.CODE.exactly().code(NewbornBilirubinNotificationService.Impl.BILIRUBIN_CODE))
                        .returnBundle(Bundle.class)
                        .execute();

        // if no bilirubin, then alert
        if (bilirubinBundle.isEmpty()) {
            EmailService.sendEmail(
                    emailUsername,
                    emailPassword,
                    emailTo,
                    emailFrom,
                    "ALERT: Newborn Bilirubin not found",
                    "<h1>Newborn patient without Bilirubin observation</h1>"
            );
        }
    }
}
