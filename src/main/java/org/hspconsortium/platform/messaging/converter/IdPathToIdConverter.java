package org.hspconsortium.platform.messaging.converter;

import ca.uhn.fhir.model.primitive.IdDt;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.GenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class IdPathToIdConverter extends AbstractHttpMessageConverter<IdDt>
            implements GenericHttpMessageConverter<IdDt> {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return canRead(clazz, null, mediaType);
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        boolean canRead = true;

        // can only read application/json
        if (mediaType != null) {
            canRead = mediaType.isCompatibleWith(MediaType.TEXT_PLAIN);
        }

        return canRead;
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return false;
//        boolean canWrite = true;
//
//         can only read application/json
//        if (mediaType != null) {
//            canWrite = mediaType.isCompatibleWith(MediaType.TEXT_PLAIN);
//        }
//
//        return canWrite;
    }

    @Override
    public boolean canWrite(Type type, Class<?> aClass, MediaType mediaType) {
        return false;
//        boolean canWrite = true;
//
//         can only read application/json
//        if (mediaType != null) {
//            canWrite = mediaType.isCompatibleWith(MediaType.TEXT_PLAIN);
//        }
//
//        return canWrite;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        // should not be called, since we override canRead/Write instead
        throw new UnsupportedOperationException();
    }

    @Override
    protected IdDt readInternal(Class<? extends IdDt> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        String stringBody = IOUtils.toString(inputMessage.getBody(), DEFAULT_CHARSET);
        return transform(stringBody);
    }

    @Override
    public IdDt read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        String stringBody = IOUtils.toString(inputMessage.getBody(), DEFAULT_CHARSET);
        return transform(stringBody);
    }

    @Override
    public void write(IdDt idDt, Type type, MediaType mediaType, HttpOutputMessage httpOutputMessage) throws IOException, HttpMessageNotWritableException {
        writeInternal(idDt, httpOutputMessage);
    }

    @Override
    protected void writeInternal(IdDt idDt, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        outputMessage.getBody().write(idDt.toString().getBytes(DEFAULT_CHARSET));
    }

    public IdDt transform(String idPath) {
        Validate.notNull(idPath);
        // expecting "{ResourceTypeName}/{ID}"
        // ex: Observation/1234
        String[] tokens = idPath.split("/");
        Validate.isTrue(tokens.length == 2);
        return new IdDt(tokens[1]);
    }

    @Override
    protected MediaType getDefaultContentType(IdDt idDt) throws IOException {
        return MediaType.TEXT_PLAIN;
    }

    @Override
    protected Long getContentLength(IdDt idDt, MediaType contentType) throws IOException {
        return super.getContentLength(idDt, contentType);
    }
}
